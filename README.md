# Snooker-ICAL
Creates an ICAL file from the event information on api.snooker.org

Takes an argument of a year. So for the 2016/17 season, you would call "python snookerical.py --season 2016".

Returns a year either side, so "python snookerical.py --season 2016" would return 2015/16, 2016/17, 2017/18.

ftpcreds contains 4 lines - hostname, username, password, directory. Not included in repo for obvious reasons!

Written for python2.7.

Hosted ICS (to add as a URL to google calendar) available at http://www.geekynick.co.uk/snooker.ics and updated daily by cron using this script.

NOTE: The version of ics listed in requirements.txt has a bug with all day events - the hosted version above uses the package straight from source on github (https://github.com/C4ptainCrunch/ics.py)
