#!/usr/bin/env python
"""
Import calendar data from api.snooker.org (provided as JSON) and output as ICAL.
# Author Nick Shaw - www.alwaysnetworks.co.uk
# Blog - www.geekynick.co.uk

Pass in a year, it will also give a year either side, always returning 3 years.
"""

import urllib2
import json
from ics import Calendar, Event
import sys
import argparse
from datetime import datetime
import filecmp
import shutil
from ftplib import FTP
import os


now = datetime.now()


parser = argparse.ArgumentParser(description='Import calendar data from api.snooker.org (provided as JSON) and output as ICAL. Author: Nick Shaw, www.alwaysnetworks.co.uk')
parser.add_argument('--season', default=str(now.year), help='Which season to download information for. i.e. for 2016/17, enter "2016". Default is this year.')
args=parser.parse_args()

reload(sys)
sys.setdefaultencoding('utf8')


#Get the calendars

req_headers = {"X-Requested-By": "geekynickSnookerICAL"}

year_requested = args.season
year_before = str(int(args.season) - 1)
year_after = str(int(args.season) + 1)

years = [year_requested, year_before, year_after]

c = Calendar()

for year in years:
    url = "http://api.snooker.org/?t=5&s=" + year
    
    req = urllib2.Request(url, headers=req_headers)
    response = urllib2.urlopen(req)

    jsoncal = json.loads(response.read())
    
    for line in jsoncal:
        e = Event()
        e.name = line["Name"]
        e.begin = line["StartDate"]
        e.end = line["EndDate"]
        e.location = line["Venue"] + ", " + line["City"] + ", " + line["Country"]
        e.make_all_day()
        c.events.add(e)

with open(os.path.join(sys.path[0], 'snooker.ics'), 'w') as f:
    f.writelines(c)

with open(os.path.join(sys.path[0], 'ftpcreds'), 'r') as f:
    creds = f.readlines()
ftp = FTP(creds[0].rstrip(), creds[1].rstrip(), creds[2].rstrip())
ftp.cwd(creds[3].rstrip())
ftp.storbinary('STOR snooker.ics', open(os.path.join(sys.path[0], 'snooker.ics'), 'rb'))
ftp.close()
